<?php

namespace App\Entity;

use App\Repository\CampervanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"campervan:read"}},
 *      denormalizationContext={"groups"={"campervan:write"}}
 * )
 * @ORM\Entity(repositoryClass=CampervanRepository::class)
 */
class Campervan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"campervan:read", "campervan:write", "order:read"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="campervans")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"campervan:read", "campervan:write"})
     */
    private $station;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"campervan:read", "campervan:write", "order:read"})
     */
    private $available;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="campervan")
     */
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCampervan($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCampervan() === $this) {
                $order->setCampervan(null);
            }
        }

        return $this;
    }
}
