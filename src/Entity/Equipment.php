<?php

namespace App\Entity;

use App\Repository\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ApiResource(
 *      normalizationContext={"groups"={"equipment:read"}},
 *      denormalizationContext={"groups"={"equipment:write"}}
 * )
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 */
class Equipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"equipment:read", "equipment:write", "order:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=StationEquipment::class, mappedBy="equipment")
     */
    private $equipmentStations;

    /**
     * @ORM\OneToMany(targetEntity=OrderEquipment::class, mappedBy="equipment")
     */
    private $ordersInEquipment;

    public function __construct()
    {
        $this->equipmentStations = new ArrayCollection();
        $this->ordersInEquipment = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|StationEquipment[]
     */
    public function getEquipmentStations(): Collection
    {
        return $this->equipmentStations;
    }

    public function addEquipmentStation(StationEquipment $equipmentStation): self
    {
        if (!$this->equipmentStations->contains($equipmentStation)) {
            $this->equipmentStations[] = $equipmentStation;
            $equipmentStation->setEquipment($this);
        }

        return $this;
    }

    public function removeEquipmentStation(StationEquipment $equipmentStation): self
    {
        if ($this->equipmentStations->removeElement($equipmentStation)) {
            // set the owning side to null (unless already changed)
            if ($equipmentStation->getEquipment() === $this) {
                $equipmentStation->setEquipment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderEquipment[]
     */
    public function getOrdersInEquipment(): Collection
    {
        return $this->ordersInEquipment;
    }

    public function addOrdersInEquipment(OrderEquipment $ordersInEquipment): self
    {
        if (!$this->ordersInEquipment->contains($ordersInEquipment)) {
            $this->ordersInEquipment[] = $ordersInEquipment;
            $ordersInEquipment->setEquipment($this);
        }

        return $this;
    }

    public function removeOrdersInEquipment(OrderEquipment $ordersInEquipment): self
    {
        if ($this->ordersInEquipment->removeElement($ordersInEquipment)) {
            // set the owning side to null (unless already changed)
            if ($ordersInEquipment->getEquipment() === $this) {
                $ordersInEquipment->setEquipment(null);
            }
        }

        return $this;
    }
}
