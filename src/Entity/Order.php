<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"order:read"}},
 *      denormalizationContext={"groups"={"order:write"}}
 * )
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Campervan::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"order:read", "order:write"})
     */
    private $campervan;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="ordersStartFrom")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"order:read", "order:write"})
     */
    private $startStation;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="ordersEndFrom")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"order:read", "order:write"})
     */
    private $endStation;

    /**
     * @ORM\Column(type="date")
     * @Groups({"order:read", "order:write"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"order:read", "order:write"})
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity=OrderEquipment::class, mappedBy="rentalOrder", cascade={"persist"})
     * @Groups({"order:read", "order:write"})
     */
    private $orderEquipments;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"order:read"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->orderEquipments = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getStartStation(): ?Station
    {
        return $this->startStation;
    }

    public function setStartStation(?Station $startStation): self
    {
        $this->startStation = $startStation;

        return $this;
    }

    public function getEndStation(): ?Station
    {
        return $this->endStation;
    }

    public function setEndStation(?Station $endStation): self
    {
        $this->endStation = $endStation;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection|OrderEquipment[]
     */
    public function getOrderEquipments(): Collection
    {
        return $this->orderEquipments;
    }

    public function addOrderEquipment(OrderEquipment $orderEquipment): self
    {
        if (!$this->orderEquipments->contains($orderEquipment)) {
            $this->orderEquipments[] = $orderEquipment;
            $orderEquipment->setRentalOrder($this);
        }

        return $this;
    }

    public function removeOrderEquipment(OrderEquipment $orderEquipment): self
    {
        if ($this->orderEquipments->removeElement($orderEquipment)) {
            // set the owning side to null (unless already changed)
            if ($orderEquipment->getRentalOrder() === $this) {
                $orderEquipment->setRentalOrder(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
