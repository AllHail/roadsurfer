<?php

namespace App\Entity;

use App\Repository\OrderEquipmentRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      collectionOperations={},
 *      itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass=OrderEquipmentRepository::class)
 */
class OrderEquipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderEquipments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"order:write"})
     */
    private $rentalOrder;

    /**
     * @ORM\ManyToOne(targetEntity=Equipment::class, inversedBy="ordersInEquipment")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"order:read", "order:write"})
     */
    private $equipment;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order:read", "order:write"})
     */
    private $bookedCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentalOrder(): ?Order
    {
        return $this->rentalOrder;
    }

    public function setRentalOrder(?Order $rentalOrder): self
    {
        $this->rentalOrder = $rentalOrder;

        return $this;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getBookedCount(): ?int
    {
        return $this->bookedCount;
    }

    public function setBookedCount(int $bookedCount): self
    {
        $this->bookedCount = $bookedCount;

        return $this;
    }
}
