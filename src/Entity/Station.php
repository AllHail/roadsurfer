<?php

namespace App\Entity;

use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"station:read"}},
 *      denormalizationContext={"groups"={"station:write"}}
 * )
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"station:read", "station:write", "order:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Campervan::class, mappedBy="station")
     */
    private $campervans;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="startStation")
     */
    private $ordersStartFrom;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="endStation")
     */
    private $ordersEndFrom;

    /**
     * @ORM\OneToMany(targetEntity=StationEquipment::class, mappedBy="station")
     */
    private $stationEquipments;

    public function __construct()
    {
        $this->campervans = new ArrayCollection();
        $this->ordersStartFrom = new ArrayCollection();
        $this->ordersEndFrom = new ArrayCollection();
        $this->stationEquipments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Campervan[]
     */
    public function getCampervans(): Collection
    {
        return $this->campervans;
    }

    public function addCampervan(Campervan $campervan): self
    {
        if (!$this->campervans->contains($campervan)) {
            $this->campervans[] = $campervan;
            $campervan->setStation($this);
        }

        return $this;
    }

    public function removeCampervan(Campervan $campervan): self
    {
        if ($this->campervans->removeElement($campervan)) {
            // set the owning side to null (unless already changed)
            if ($campervan->getStation() === $this) {
                $campervan->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersStartFrom(): Collection
    {
        return $this->ordersStartFrom;
    }

    public function addOrdersStartFrom(Order $ordersStartFrom): self
    {
        if (!$this->ordersStartFrom->contains($ordersStartFrom)) {
            $this->ordersStartFrom[] = $ordersStartFrom;
            $ordersStartFrom->setStartStation($this);
        }

        return $this;
    }

    public function removeOrdersStartFrom(Order $ordersStartFrom): self
    {
        if ($this->ordersStartFrom->removeElement($ordersStartFrom)) {
            // set the owning side to null (unless already changed)
            if ($ordersStartFrom->getStartStation() === $this) {
                $ordersStartFrom->setStartStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersEndFrom(): Collection
    {
        return $this->ordersEndFrom;
    }

    public function addOrdersEndFrom(Order $ordersEndFrom): self
    {
        if (!$this->ordersEndFrom->contains($ordersEndFrom)) {
            $this->ordersEndFrom[] = $ordersEndFrom;
            $ordersEndFrom->setEndStation($this);
        }

        return $this;
    }

    public function removeOrdersEndFrom(Order $ordersEndFrom): self
    {
        if ($this->ordersEndFrom->removeElement($ordersEndFrom)) {
            // set the owning side to null (unless already changed)
            if ($ordersEndFrom->getEndStation() === $this) {
                $ordersEndFrom->setEndStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StationEquipment[]
     */
    public function getStationEquipments(): Collection
    {
        return $this->stationEquipments;
    }

    public function addStationEquipment(StationEquipment $stationEquipment): self
    {
        if (!$this->stationEquipments->contains($stationEquipment)) {
            $this->stationEquipments[] = $stationEquipment;
            $stationEquipment->setStation($this);
        }

        return $this;
    }

    public function removeStationEquipment(StationEquipment $stationEquipment): self
    {
        if ($this->stationEquipments->removeElement($stationEquipment)) {
            // set the owning side to null (unless already changed)
            if ($stationEquipment->getStation() === $this) {
                $stationEquipment->setStation(null);
            }
        }

        return $this;
    }
}
