<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * @ApiResource()
 */
class Timeline
{
   /**
     * @ApiProperty(identifier=true)
     */
    public $date;
    public $test;
    public $totalVisitors;
    public $mostPopularListings;

 
}