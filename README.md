Roadsurfer Application
========================

Requirements
------------

  * PHP 7.2.5 or higher;
  * PDO-MYSQL PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

[Download Symfony][5] to install the `symfony` binary on your computer and run
this command:

you can use Composer:

```bash
$ composer update
```

Usage
-----

There's no need to configure anything to run the application. If you have
[installed Symfony][5] binary, run this command:

```bash
$ cd roadsurfer/
$ symfony serve
```

Then access the application in your browser at the given URL (<https://localhost:8000> by default).

If you don't have the Symfony binary installed, run `php -S localhost:8000 -t public/`
to use the built-in PHP web server or [configure a web server][3] like Nginx or
Apache to run the application.

Tests
-----

Execute this command to run tests:

```bash
$ cd roadsurfer/
$ ./bin/phpunit
```

[1]: https://symfony.com/doc/current/best_practices.html
[2]: https://symfony.com/doc/current/reference/requirements.html
[3]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html
[4]: https://symfony.com/download